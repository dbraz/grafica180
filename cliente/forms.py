#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from models import *
from produto.models import *


class formTeste(forms.Form):
    nome = forms.CharField(label='Titulo',max_length=200, required=True,widget=forms.TextInput(attrs={'class':'input-xlarge focused'}))
    img = forms.Field(label='Imagem',required = False,help_text="Foto destaque do treinamento. Dimensoes 808X370",widget=forms.FileInput(attrs={"class":"input-file uniform_on","id":"fileInput"}))


class formCadProduto(forms.Form):
    nome = forms.CharField(label='Titulo',max_length=200, required=True,widget=forms.TextInput(attrs={'class':'input-xlarge focused'}))
    quantidade = forms.CharField(label='Quantidade',max_length=10, required=True,widget=forms.TextInput(attrs={'class':'input-xlarge focused'}))
    categoria = forms.ChoiceField(label='Atividade',choices=CATEGORIA)
    imagem_apresentacao = forms.Field(label='Imagem',required = False,help_text="Foto destaque do treinamento. Dimensoes 808X370",widget=forms.FileInput(attrs={"class":"input-file uniform_on ajuste","id":"fileInput"}))
    valor = forms.CharField(label='Valor',max_length=10, required=True,widget=forms.TextInput(attrs={'class':'input-xlarge focused'}))
    descricao = forms.CharField(widget = forms.Textarea)

class formLogin(forms.Form):
    nome = forms.CharField(label='texto',widget = forms.Textarea)
    senha = forms.CharField(max_length=32, widget=forms.PasswordInput)
    email = forms.CharField(label='email',max_length=200)