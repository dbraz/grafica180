# Create your models here.
# -*- coding: utf-8 -*-
from django.db import models
# from django.contrib.localflavor.br.br_states import STATE_CHOICES
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from datetime import *

STATE_CHOICES = []

ESCOLHA_C = ((True, 'Sim'),(False, 'Não'),)

SEXO_CHOICES = (('F','Feminino'),('M','Masculino'),)

IDIOMA_CHOICES = (
    ('Alemao', 'Alemão'), ('Espanhol', 'Espanhol'),
    ('Frances', 'Francês'), ('Ingles', 'Inglês'),
    ('Italiano', 'Italiano'), ('Japones', 'Japonês'))


def foto_upload(instance, filename):
    filename = filename.split('.')
    ext = filename[1]
    filename = filename[0]
    nome_arquivo = "%s.%s" % (slugify(filename),ext)
    return '/'.join(['fotos', nome_arquivo])


class Cliente(models.Model):
    user = models.ForeignKey(User)
    nome = models.CharField('Nome Completo', max_length=100)
    nacionalidade = models.CharField('Nacionalidade', max_length=50)
    nascimento = models.DateField('Data de Nascimento', blank=True, null=True)
    foto = models.ImageField('Envie uma foto', upload_to=foto_upload, blank = True, null = True)
    sexo = models.CharField('Sexo', max_length=1, choices=SEXO_CHOICES)
    rg = models.CharField('RG', max_length=50, blank=True)
    cpf = models.CharField('CPF', max_length=14)
    telefone1 = models.CharField('Telefone', max_length=13)
    telefone2 = models.CharField('Telefone (Opcional)', max_length=13, blank=True)
    data_cadastro = models.DateField(auto_now_add=True)

    def get_fields(self):
        dados = [(field, field.value_to_string(self)) for field in Curriculo._meta.fields if field.name != "id" and field.name != "user"]
        dados[2] = [dados[2][0], datetime.strptime(dados[2][1], "%Y-%m-%d").date()]
        if dados[4][1] == 'M':
            dados[4] = [dados[4][0], SEXO_CHOICES[1][1]]
        else:
            dados[4] = [dados[4][0], SEXO_CHOICES[0][1]]
        dados[5] = [dados[5][0], ESTADOCIVIL_C[int(dados[5][1])][1]]
        url = dados[11][1].split("=")[-1]
        dados[11] = [dados[11][0], url]
        dados[12] = [dados[12][0], datetime.strptime(dados[12][1], "%Y-%m-%d").date()]
        return dados

    def get_idade(self):
        try:
            return (date.today() - self.nascimento).days / 365
        except:
            return self.nascimento
    get_idade.short_description = "Idade"

    # def get_fields(self):
    #     return [(field, field.value_to_string(self)) for field in Curriculo._meta.fields]

    def get_endereco(self):
        return self.user.endereco_usuario.formatado()

    def get_idiomas(self):
        return self.idioma_set.all()

    def get_comentario(self):
        return self.usuario_set.all()[0].comentario

    def __unicode__(self):
        return self.nome

    def set_datafb(self, data):
        curriculo = Curriculo()
        curriculo.nascimento = data['']


class Endereco(models.Model):
    usuario = models.OneToOneField(User, related_name = "endereco_usuario")
    endereco = models.CharField(max_length=100)
    bairro = models.CharField(max_length=100)
    complemento = models.CharField(max_length=100, blank = True, null = True)
    cep = models.CharField('CEP', max_length=20)
    cidade = models.ForeignKey('Cidade', blank=True, null = True)

    def get_fields(self):
        endereco = [(field, field.value_to_string(self)) for field in Endereco._meta.fields if field.name != "id" and field.name != "usuario"]
        c = Cidade.objects.get(id=endereco[4][1])
        e = [endereco[4][0], c]
        endereco[4] = e
        return endereco

    # def get_fields(self):
    #     return [(field, field.value_to_string(self)) for field in Endereco._meta.fields]

    def __unicode__(self):
        return self.endereco


class Estado(models.Model):
    uf = models.CharField('Sigla', max_length=2)
    nome = models.CharField('Estado', max_length=50, choices=STATE_CHOICES)

    @staticmethod
    def get_cidades(id):
        cidades = Cidade.objects.filter(estado_id=id)
        c = []
        for cidade in cidades:
            c.append({'id': cidade.id, 'nome': cidade.nome})
        return c

    def __unicode__(self):
        return self.nome


class Cidade(models.Model):
    estado = models.ForeignKey('Estado')
    nome = models.CharField('Cidade', max_length=100)

    def __unicode__(self):
        return self.nome


class Usuario(models.Model):
    cliente = models.ForeignKey('Cliente')
    usuario = models.OneToOneField(User, blank=True, null=True)
    comentario = models.CharField("Caso queria, envie seu comentário ou alguma dúvida", max_length=200, null=True, blank=True)
    confirmacao = models.CharField(max_length=500)

    def get_fields(self):
        return [(field, field.value_to_string(self)) for field in Usuario._meta.fields]