from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from filebrowser.sites import site
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'venda2.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    (r'^media/(.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', 'front.views.home', name='home'),
    url(r'^teste', 'front.views.teste', name='teste'),
    url(r'^categoria', 'front.views.categoria', name='categoria'),
    url(r'^imagem', 'front.views.testeImagem', name='testeImagem'),
    url(r'^all_products', 'front.views.all_products', name='todos os produtos'),
    url(r'^login', 'front.views.sendMail', name='testeLogin'),
    url(r'^detalhe_produto', 'front.views.detalhe_produto', name='home'),
    url(r'^admin/', include(admin.site.urls)),
)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
