from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from cliente.models import *
from cliente.forms import *
from produto.models import *
from produto.util import *
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
import json


def home(request):
	a = Produtos.objects.all()[:8]
	return render_to_response("index.html",{"produtos":a},context_instance=RequestContext(request)) 	


def detalhe_produto(request):
	id_produto = request.GET['id']
	produto_selecionado = Produtos.objects.get(id=id_produto)

	return render_to_response("page-product-details.html",{'produto':produto_selecionado},context_instance=RequestContext(request)) 


def teste(request):
    
    if request.method == "POST":
        form = formTeste(request.POST, request.FILES)
        if form.is_valid():
            tre = Teste(
                nome=form.cleaned_data['nome'],
                imagem = form.cleaned_data['img'],
                )
            tre.save()
            return HttpResponseRedirect('/imagem')
    else:
        form = formTeste()

    return render_to_response(
        "teste.html", {
            'form': form,
            'nome_form': 'Adicionar Treinamento',
        },
        context_instance=RequestContext(request))


def testeImagem(request):
	if request.method == "POST":
		print request
		form = formCadProduto(request.POST, request.FILES)
		if form.is_valid():
			tre = Produtos(
                nome = form.cleaned_data['nome'],
                imagem_apresentacao = form.cleaned_data['imagem_apresentacao'],
                quantidade = form.cleaned_data['quantidade'],
                valor = form.cleaned_data['valor'],
                categoria = form.cleaned_data['categoria'],
                descricao = form.cleaned_data['descricao'],
                )
			tre.save()
			return HttpResponseRedirect('/imagem')
	else:
		form = formCadProduto()
	return render_to_response("galeria.html",{'form': form},context_instance = RequestContext(request))


def all_products(request):
	all_produtos = Produtos.objects.all()
	selecao = []
	for c in CATEGORIA:
		select = Produtos.objects.filter(categoria=c[0])[:4]
		selecao+=select
	return render_to_response("page-products-4-columns.html",
								{"categoria":CATEGORIA,
								"produtos":selecao},
								context_instance = RequestContext(request))


def categoria(request):
	products_select = Produtos.objects.filter(categoria=str(request.GET['index']))
	return render_to_response("categoria.html",
								{"categoriaSelecionada":get_categoria(str(request.GET['index'])),
								"produtos":products_select},
								context_instance = RequestContext(request))	


def sendMail(request):
	form = formLogin(request.POST or None)
	if form.is_valid():
		# save_it = form.save(commit=False)
		# save_it.save()
		subject = 'mensagem'
		message = request.POST['nome']
		from_email = settings.EMAIL_HOST_USER
		to_list = [request.POST['email']]

		send_mail(subject,message,from_email,to_list,fail_silently=True)

		messages.success(request,'mensagem')
		return HttpResponseRedirect('/login')
	return render_to_response("testeLogin.html",
								{"formLogin":form},
								context_instance = RequestContext(request))	
	