#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models
from datetime import *
from cliente.models import *

CATEGORIA = (('1','BANER'),('2','INGRESSOS'),('3','PULSERAS'),)

class Produtos(models.Model):
    nome = models.CharField(max_length=30)  #nome Produto
    valor = models.CharField(max_length=30) #valor do produto
    quantidade = models.CharField(max_length=30) #quantidade do produto
    categoria = models.CharField('Sexo', max_length=1, choices=CATEGORIA)
    imagem_apresentacao = models.ImageField(upload_to=imagem_upload, blank=True)
    descricao = models.TextField(blank=True)
    def get_imagem(self):
        return '/media/%s' % self.imagem_apresentacao
    
    def __unicode__(self):
        return self.nome


class Imagem(models.Model):

    class Meta:
        ordering = ('produto','titulo',)

    produto = models.ForeignKey('Produtos')
    titulo = models.CharField(max_length=100)
    descricao = models.TextField(blank=True)
    imagem = models.ImageField(upload_to=imagem_upload, blank=True, help_text ="Foto do Treinamento. Dimensões 808X370")

    def get_imagem(self):
        return '/media/%s' % self.imagem

    def __unicode__(self):
        return self.titulo


class Pedido(models.Model):
	num_pedido = models.CharField(max_length=30,)  #numero Pedido
	cadastrado_em = models.DateTimeField(default=datetime.now)
	cliente = models.ForeignKey('cliente.Cliente')
	def __unicode__(self):
		return self.num_pedido


class Comentario(models.Model):
	produto = models.ForeignKey('Produtos')
	cliente = models.ForeignKey('cliente.Cliente')	
	descricao = models.TextField(blank=True)
	def __unicode__(self):
		return self.descricao


class ProdutoSelecionado(models.Model):
	nome = models.CharField(max_length=30)  #nome Produto
	valor = models.CharField(max_length=30) #valor do produto
	imagem = models.ImageField(upload_to=imagem_upload, blank=True)
	pedido = models.ForeignKey('Pedido')
	def __unicode__(self):
		return self.nome





