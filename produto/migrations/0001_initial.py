# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Produtos'
        db.create_table(u'produto_produtos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('valor', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('quantidade', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('categoria', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('imagem_apresentacao', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'produto', ['Produtos'])

        # Adding model 'Imagem'
        db.create_table(u'produto_imagem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('produto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['produto.Produtos'])),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('descricao', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'produto', ['Imagem'])

        # Adding model 'Pedido'
        db.create_table(u'produto_pedido', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('num_pedido', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliente.Cliente'])),
        ))
        db.send_create_signal(u'produto', ['Pedido'])

        # Adding model 'Comentario'
        db.create_table(u'produto_comentario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('produto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['produto.Produtos'])),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cliente.Cliente'])),
            ('descricao', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'produto', ['Comentario'])

        # Adding model 'ProdutoSelecionado'
        db.create_table(u'produto_produtoselecionado', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('valor', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('pedido', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['produto.Pedido'])),
        ))
        db.send_create_signal(u'produto', ['ProdutoSelecionado'])


    def backwards(self, orm):
        # Deleting model 'Produtos'
        db.delete_table(u'produto_produtos')

        # Deleting model 'Imagem'
        db.delete_table(u'produto_imagem')

        # Deleting model 'Pedido'
        db.delete_table(u'produto_pedido')

        # Deleting model 'Comentario'
        db.delete_table(u'produto_comentario')

        # Deleting model 'ProdutoSelecionado'
        db.delete_table(u'produto_produtoselecionado')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'cliente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'data_cadastro': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nacionalidade': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'nascimento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rg': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'telefone1': ('django.db.models.fields.CharField', [], {'max_length': '13'}),
            'telefone2': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'produto.comentario': {
            'Meta': {'object_name': 'Comentario'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliente.Cliente']"}),
            'descricao': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['produto.Produtos']"})
        },
        u'produto.imagem': {
            'Meta': {'ordering': "('produto', 'titulo')", 'object_name': 'Imagem'},
            'descricao': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'produto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['produto.Produtos']"}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'produto.pedido': {
            'Meta': {'object_name': 'Pedido'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cliente.Cliente']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_pedido': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'produto.produtos': {
            'Meta': {'object_name': 'Produtos'},
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem_apresentacao': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'quantidade': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'valor': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'produto.produtoselecionado': {
            'Meta': {'object_name': 'ProdutoSelecionado'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'pedido': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['produto.Pedido']"}),
            'valor': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['produto']